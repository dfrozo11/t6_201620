package taller.test;

import junit.framework.TestCase;
import taller.estructuras.TablaHash;
import taller.estructuras.nuevaTabla;

public class testNuevaTabla extends TestCase
{

	private nuevaTabla<String, String> tabla;


	private void setupscenario1()
	{

		tabla = new nuevaTabla<String, String>(2000, 60);

	}

	private void setupscenario2()
	{
		tabla.put("201426137", "Sara");
		tabla.put("201426138", "Paola");

		for ( int i =1;i<1000;i++)
		{
			tabla.put((201426138+i)+"", "usuario"+i);
		}

	}

	public void testAgregarDato()
	{
		setupscenario1();
		tabla.put("201426137", "Daniel");
		assertEquals("La tabla no esta agregando elementos correctamente", 1, tabla.size());

		setupscenario2();
		assertEquals("La tabla no esta agregando elementos correctamente", 1001, tabla.size());
	}

	public void testObtenerDato()
	{
		setupscenario1();
		assertNull("El arreglo deberia devolver null", tabla.get("201426137"));

		tabla.put("201426137", "Daniel");
		assertNotNull("La Tabla deberia deolver un elemento", tabla.get("201426137").get(0));
		assertEquals("El valor devuelto no es el correcto", "Daniel",tabla.get("201426137").get(0));
		assertEquals("EL tamaño de la Tabla no deberia cambiar", 1, tabla.size());
		assertNull("El arreglo deberia devolver null", tabla.get("201426138"));

		
		setupscenario2();
		assertNotNull("La Tabla deberia devolver un elemento", tabla.get("201426137"));
		assertEquals("los valores devueltos no son los correctos", "Daniel",tabla.get("201426137").get(0));
		assertEquals("los valores devueltos no son los correctos", "Sara",tabla.get("201426137").get(1));
		
	}

	public void testEliminarDato()
	{
		setupscenario1();
		setupscenario2();

		tabla.delete("201426137");
		assertEquals("El tamaño de la Tabla  deberia cambiar", 1000, tabla.size());
		


	}

}








