package taller.test;

import junit.framework.TestCase;
import taller.estructuras.TablaHash;

public class testTablaHash extends TestCase
{
	
	private TablaHash tabla;
	
	
	private void setupscenario1()
	{
		
		tabla = new TablaHash<String, String>();
		
	}
	
	private void setupscenario2()
	{
		tabla.put("201426137", "Sara");
		tabla.put("201426138", "Paola");
		
		for ( int i =1;i<1000;i++)
		{
			tabla.put((201426138+i)+"", "usuario"+i);
		}
		
	}
	
	public void testAgregarDato()
	{
		setupscenario1();
		tabla.put("201426137", "Daniel");
		assertEquals("La tabla no esta agregando elementos correctamente", 1, tabla.size());
		
		setupscenario2();
		assertEquals("La tabla no esta agregando elementos correctamente", 1001, tabla.size());
	}
	
	public void testObtenerDato()
	{
		setupscenario1();
		assertNull("El arreglo deberia devolver null", tabla.get("201426137"));
		
		tabla.put("201426137", "Daniel");
		assertNotNull("La lista deberia deolver un elemento", tabla.get("201426137"));
		assertEquals("El valor devuelto no es el correcto", "Daniel",tabla.get("201426137"));
		assertEquals("EL tamaño de la lista no deberia cambiar", 1, tabla.size());
		assertNull("El arreglo deberia devolver null", tabla.get("201426138"));
		
	}
	
	public void testEliminarDato()
	{
		setupscenario1();
		setupscenario2();
		
		String nombre=(String) tabla.delete("201426137");
		assertEquals("EL tamaño de la lista  deberia cambiar", 1000, tabla.size());
		assertNotNull("El arreglo no deberia devolver null", nombre);
		assertEquals("El valor devuelto no es el correcto", "Sara",nombre);
		
		
	}
	
}
