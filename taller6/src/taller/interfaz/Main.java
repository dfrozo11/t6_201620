package taller.interfaz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import taller.estructuras.TablaHash;
import taller.estructuras.habitante;
import taller.estructuras.nuevaTabla;


public class Main {

	private static String rutaEntrada = "./data/ciudadLondres.csv";

	public static void main(String[] args) {
		TablaHash emergencias = new TablaHash<>(2*10^6,60 );
		nuevaTabla nombres= new nuevaTabla<>(2*10^6,60 );
		nuevaTabla location = new nuevaTabla<>(2*10^6,60 );
		System.out.println("Bienvenido al directorio de emergencias por colicsiones de la ciudad de Londres");
		System.out.println("Espere un momento mientras cargamos la información...");
		System.out.println("Esto puede tardar unos minutos...");
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(rutaEntrada)));
			String entrada = br.readLine();


			int i = 0;
			entrada = br.readLine();
			while (entrada != null){
				String[] datos = entrada.split(",");
				habitante nuevo  = new habitante(datos[0],datos[1], datos[2], datos[3], datos[4]);
				emergencias.put(nuevo.darNumero(), nuevo);
				nombres.put(nuevo.darNombre(), nuevo);
				location.put(nuevo.darLatitud()+","+nuevo.darLongitud(), nuevo);
				++i;
				if (++i%500000 == 0)
					System.out.println(i+" entradas...");

				entrada = br.readLine();
			}
			System.out.println(i+" entradas cargadas en total");
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Directorio de emergencias por colisiones de Londres v1.0");

		boolean seguir = true;

		while (seguir)
			try {
				System.out.println("Bienvenido, seleccione alguna opcion del menú a continuación:");
				System.out.println("1: Agregar ciudadano a la lista de emergencia");
				System.out.println("2: Buscar ciudadano por número de contacto del acudiante");
				System.out.println("3: Buscar ciudadano por apellido");
				System.out.println("4: Buscar ciudadano por su locaclización actual");
				System.out.println("Exit: Salir de la aplicación");

				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				String in = br.readLine();
				switch (in) {
				case "1":
					System.out.println("Ingrese el Nombre del ciudadano");
					String nombre=  br.readLine();
					System.out.println("Ingrese el apellido del ciudadano");
					String apellido=  br.readLine();
					System.out.println("Ingrese el numero del ciudadano");
					String num=  br.readLine();
					System.out.println("Ingrese la latitud del ciudadano");
					String lat=  br.readLine();
					System.out.println("Ingrese la longitud del ciudadano");
					String lon=  br.readLine();
					habitante  aAgregar=  new habitante(nombre, apellido, num, lat ,lon);
					emergencias.put(aAgregar.darNumero(), aAgregar);
					break;
				case "2":
					System.out.println("ingrese el numero de telefono de contacto:");
					String numero= br.readLine();
					float telBuscado = Float.parseFloat(numero);
					habitante buscado=(habitante) emergencias.get(telBuscado);
					if(buscado==null ){
						System.out.println("El ususario buscado no existe");
					}
					else
					{
						System.out.println("nombre: "+ buscado.darNombre());
						System.out.println("apellido: "+ buscado.darApellido());
						System.out.println("Telefono "+ buscado.darNumero());
						System.out.println("Latitud: "+ buscado.darLatitud());
						System.out.println("Longitud: "+ buscado.darLongitud());
					}

					break;
				case "3":
					System.out.println("ingrese el nombre buscado:");
					String nom = br.readLine();
					ArrayList nombrs= nombres.get(nom);
					if(nombrs!=null)
					{
						for(int i=0;i<nombrs.size();i++)
						{
							habitante actual= (habitante) nombrs.get(i);
							System.out.println("Nombre: "+ actual.darNombre() + ", Apellido: "+ actual.darApellido()+ ", Telefono: " +actual.darNumero()+ ", Latitud: " + actual.darLatitud()+ ", Longitud "+ actual.darLongitud() );
						}
					}

					break;
				case "4":
					System.out.println("ingrese la longitud segido por una coma  y la longitud buscada: (ej :10,100)");
					String local = br.readLine();
					ArrayList personas= location.get(local);
					if(personas!=null)
					{
						for(int i=0;i<personas.size();i++)
						{
							habitante actual= (habitante) personas.get(i);
							System.out.println("Nombre: "+ actual.darNombre() + ", Apellido: "+ actual.darApellido()+ ", Telefono: " +actual.darNumero() );
						}
					}
					
					break;
				case "Exit":
					System.out.println("Cerrando directorio...");
					seguir = false;
					break;

				default:
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}


	}



}
