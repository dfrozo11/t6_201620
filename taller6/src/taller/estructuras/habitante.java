package taller.estructuras;

public class habitante 
{

	private String nombre;
	
	private String apellido;
	
	private int telefono;
	
	private double latitud;
	
	private double longitud;
	
	
	public habitante(String nombre, String apellido, String telefono, String latitud, String Longitud)
	{
		this.nombre= nombre; 
		this.apellido= apellido;
		this.telefono= Integer.parseInt(telefono);
		this.latitud= Integer.parseInt(latitud);
		this.longitud= Integer.parseInt(Longitud);
	}
	
	public String darNombre()
	{
		return nombre;
	}
	
	public String darApellido(){
		return apellido;
	}
	
	public int darNumero()
	{
		return telefono;
	}
	
	public double darLatitud()
	{
		return latitud;
	}
	
	public double darLongitud()
	{
		return longitud;
	}
}
