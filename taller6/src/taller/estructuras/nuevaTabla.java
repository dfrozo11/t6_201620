package taller.estructuras;

import java.util.ArrayList;

public class nuevaTabla<K, V>
{

	private class NodoLista<K,V> {

		private K llave;
		private ArrayList valor;
		private NodoLista<K, V> next;

		public NodoLista(K llave, V valor) {
			super();
			this.llave = llave;
			this.valor = new ArrayList();
			this.valor.add(valor);
			this.next= null;
		}

		public K getLlave() {
			return llave;
		}

		public void setLlave(K llave) {
			this.llave = llave;
		}

		public ArrayList<V> getValor() {
			return valor;
		}

		public void addValor(V valor) {
			this.valor.add(valor);
		}

		public NodoLista<K, V> getNext()
		{
			return next;
		}

		public void setNext(NodoLista<K, V> siguiente)
		{
			next= siguiente;
		}
	}


	private class SecuentialSearchSTLista<K,V>
	{

		private NodoLista<K, V> primero;

		private int size;

		public ArrayList<V> getValor(K llave)
		{
			for(NodoLista<K, V> x = primero;x!= null;	x = x.getNext())
			{
				if(llave.equals(x.getLlave()))
				{
					return x.getValor();
				}
			}

			return null;

		}

		public void put(K llave, V valor)
		{
			for(NodoLista<K, V> x = primero;x!= null;	x = x.getNext())
			{
				if(llave.equals(x.getLlave()))
				{
					x.addValor(valor);return;
				}

			}
			NodoLista<K, V> nuevo = new NodoLista<K,V>(llave, valor);
			nuevo.setNext(primero);
			primero= nuevo;
			size++;
		}

		public boolean delete(K llave) {

			if(primero.getLlave().equals(llave))
			{

				primero= primero.getNext();
				size--;
				return true;
			}
			for(NodoLista<K, V> x = primero;x!= null;	x = x.getNext())
			{
				if(x.getNext()!=null)
				{
					if(x.getNext().getLlave().equals(llave))
					{

						x.setNext(x.getNext().getNext());
						size--;
						return true;
					}
				}

			}
			return false;
		}

		public boolean isEmpty()
		{
			return size==0;
		}

		public int size()
		{
			return size;
		}

		public Iterable<K> keys()  {
			ArrayList llaves = new ArrayList();
			for (NodoLista<K, V> x = primero; x != null; x = x.getNext())
				llaves.add(x.getLlave());
			return llaves;
		}
	}


	private SecuentialSearchSTLista<K, V>[] arreglo;

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	//Constructores
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;


	public nuevaTabla(int capacidad, float factorCargaMax) {

		this.capacidad=capacidad;
		this.factorCargaMax=factorCargaMax;
		arreglo = new SecuentialSearchSTLista[capacidad];
		count=0;
		factorCarga=0;
		for( int i=0; i< capacidad; i++)
		{
			arreglo[i]=  new SecuentialSearchSTLista();
		}

	}

	private nuevaTabla(int Capacidad, int count)
	{
		this.capacidad=Capacidad;
		arreglo = new SecuentialSearchSTLista[capacidad];
		this.count=count;
		factorCarga=0;
		for( int i=0; i< capacidad; i++)
		{
			arreglo[i]=  new SecuentialSearchSTLista();
		}
	}

	public void put(K llave, V valor){

		ArrayList llaves = (ArrayList) llaves();
		if(!llaves.contains(llave))
		{
			count++;
			factorCarga=(float) (((double)count/(double)capacidad)*100);
		}
		arreglo[(hash(llave))].put(llave, valor);



	}

	public ArrayList get(K llave){

		return arreglo[hash(llave)].getValor(llave);
	}

	public void delete(K llave){

		int aEliminar=arreglo[hash( llave)].getValor(llave).size();
		if(arreglo[hash(llave)].delete(llave))
		{
			count-=aEliminar;
		}
		return;
	}

	//Hash
	private int hash(K llave)
	{
		return Math.abs(llave.hashCode())%capacidad;
	}

	private void resize(int cap)
	{
		nuevaTabla temp= new nuevaTabla<>(cap,count);
		ArrayList llaves= (ArrayList) this.llaves();
		for (int i=0;i<llaves.size();i++)
		{
			K llave= (K) llaves.get(i);
			temp.put(llave, this.get(llave));
		}

		this.capacidad=temp.capacidad;
		this.arreglo=temp.arreglo;
	}

	public Iterable<K> llaves()
	{
		ArrayList<K> keys = new ArrayList<>();
		for (int i=0; i<capacidad;i++)
		{
			for(K llav : arreglo[i].keys() )
			{
				keys.add(llav);
			}
		}
		return keys;
	}

	public int size()
	{
		return (int)count;
	}


}


