package taller.estructuras;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;

public class SecuentialSearchST<K,V>
{
	
	private NodoHash<K, V> primero;
	
	private int size;
	
	public V getValor(K llave)
	{
		for(NodoHash<K, V> x = primero;x!= null;	x = x.getNext())
		{
			if(llave.equals(x.getLlave()))
			{
				return x.getValor();
			}
		}
		
		return null;
		
	}
	
	public void put(K llave, V valor)
	{
		for(NodoHash<K, V> x = primero;x!= null;	x = x.getNext())
		{
			if(llave.equals(x.getLlave()))
			{
				x.setValor(valor);return;
			}
			
		}
		NodoHash<K, V> nuevo = new NodoHash<K,V>(llave, valor);
		nuevo.setNext(primero);
		primero= nuevo;
		size++;
	}

	public V delete(K llave) {
	
		if(primero.getLlave().equals(llave))
		{
			V val = primero.getValor();
			primero= primero.getNext();
			size--;
			return val;
		}
		for(NodoHash<K, V> x = primero;x!= null;	x = x.getNext())
		{
			if(x.getNext()!=null)
			{
				if(x.getNext().getLlave().equals(llave))
				{
					V val=  x.getNext().getValor();
					x.setNext(x.getNext().getNext());
					size--;
					return val;
				}
			}
			
		}
		return null;
	}
	
	public boolean isEmpty()
	{
		return size==0;
	}
	
	public int size()
	{
		return size;
	}
	
	 public Iterable<K> keys()  {
	       ArrayList llaves = new ArrayList();
	        for (NodoHash<K, V> x = primero; x != null; x = x.getNext())
	            llaves.add(x.getLlave());
	        return llaves;
	    }

}
