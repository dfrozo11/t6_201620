package taller.estructuras;

import java.util.ArrayList;

public class TablaHash<K extends Comparable<K> ,V> {

	//TODO Una enumeracioón que representa los tipos de colisiones que se pueden manejar
	public enum colisiones
	{

	}

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	private SecuentialSearchST<K, V>[] arreglo;

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	//Constructores
	

	@SuppressWarnings("unchecked")
	public TablaHash()
	{
		this(1000,(float) 50);
	}

	@SuppressWarnings("unchecked")
	public TablaHash(int capacidad, float factorCargaMax) {

		this.capacidad=capacidad;
		this.factorCargaMax=factorCargaMax;
		arreglo = new SecuentialSearchST[capacidad];
		count=0;
		factorCarga=0;
		for( int i=0; i< capacidad; i++)
		{
			arreglo[i]=  new SecuentialSearchST<>();
		
		}

	}

	private TablaHash(int Capacidad, int count)
	{
		this.capacidad=Capacidad;
		arreglo = new SecuentialSearchST[capacidad];
		this.count=count;
		factorCarga=0;
		for( int i=0; i< capacidad; i++)
		{
			arreglo[i]=  new SecuentialSearchST<>();
		}
	}

	public void put(K llave, V valor){

		ArrayList llaves = (ArrayList) llaves();
		if(!llaves.contains(llave))
		{
			count++;
			factorCarga=(float) (((double)count/(double)capacidad)*100);
		}
		arreglo[(hash(llave))].put(llave, valor);

		
		
	}

	public V get(K llave){

		return arreglo[hash(llave)].getValor(llave);
	}

	public V delete(K llave){

		V eliminado=arreglo[hash(llave)].delete(llave);
		if(eliminado!=null)
		{
			count--;
			factorCarga=(float) (((double)count/(double)capacidad)*100);
		}
		
		return eliminado;
	}

	//Hash
	private int hash(K llave)
	{
		return Math.abs(llave.hashCode())%capacidad;
	}

	private void resize(int cap)
	{
		TablaHash<K, V> temp= new TablaHash<>(cap,count);
		ArrayList llaves= (ArrayList) this.llaves();
		for (int i=0;i<llaves.size();i++)
		{
			K llave= (K) llaves.get(i);
			temp.put(llave, this.get(llave));
		}

		this.capacidad=temp.capacidad;
		this.arreglo=temp.arreglo;
	}

	public Iterable<K> llaves()
	{
		ArrayList<K> keys = new ArrayList<>();
		for (int i=0; i<capacidad;i++)
		{
			for(K llav : arreglo[i].keys() )
			{
				keys.add(llav);
			}
		}
		return keys;
	}
	
	public int size()
	{
		return (int)count;
	}


}